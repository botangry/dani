import React, { FC } from 'react'
import { Route, Switch, useLocation } from 'react-router-dom'
import { animated, config, useTransition } from 'react-spring'
import styled from 'styled-components'
import { routes } from '../../routes/routes'
import { Home } from '../../pages/Home'
import { Games } from '../../pages/Games'
import { Videos } from '../../pages/Videos'
import { Contact } from '../../pages/Contact'

const PageWrapper = styled(animated.main)`
  padding: 100px 0;
  min-height: 100vh;

  display: flex;

  h1 {
    margin-bottom: 1rem;
    color: ${(p) => p.theme.color.accent};
  }

  > * {
    margin: 0 auto;
    width: 1000px;
    max-width: 1000px;

    flex: 1;
  }

  @media (max-width: 900px) {
    padding: 80px 15px;

    h1 {
      font-size: 1rem;
    }

    > * {
      width: 100%;
      max-width: 100%;
    }
  }
`

export const Pages: FC = () => {
  const location = useLocation()

  const transitions = useTransition(location, (location) => location.pathname, {
    from: { opacity: 0, position: 'absolute', transform: 'translateX(100%)' },
    enter: { opacity: 1, position: 'relative', transform: 'translateX(0)' },
    leave: { opacity: 0, position: 'absolute', transform: 'translateX(-25%)' },
    config: config.stiff,
    initial: null
  })

  return (
    <>
      {transitions.map(({ item, props, key }) => (
        <PageWrapper style={props} key={key}>
          <Switch location={item}>
            <Route path={routes.home} component={Home} exact />

            <Route path={routes.games} component={Games} />

            <Route path={routes.videos} component={Videos} />

            <Route path={routes.contact} component={Contact} />
          </Switch>
        </PageWrapper>
      ))}
    </>
  )
}
