import React, { FC } from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  padding: 1rem 2rem;

  width: 100%;
  height: 100%;

  display: flex;
  justify-content: center;
  align-items: center;

  color: #fff;
  font-size: 10rem;

  @media (max-width: 900px) {
    font-size: 5rem;
  }
`

interface IComponentProps {}

export const Logo: FC<IComponentProps> = () => <Wrapper children="Dani !" />
