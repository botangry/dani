import React from 'react'
import ReactDom from 'react-dom'
import { App } from './App'
import { BrowserRouter } from 'react-router-dom'
import { theme } from './styles/theme'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle } from './styles/GlobalStyle'
import './index.html'

ReactDom.render(
  <BrowserRouter>
    <ThemeProvider theme={theme}>
      <GlobalStyle />

      <App />
    </ThemeProvider>
  </BrowserRouter>,
  document.getElementById('app')
)
