import React, { FC } from 'react'
import styled from 'styled-components'
import { Card } from '../common/Card'

const StyledCard = styled(Card)``

interface IComponentProps {
  data: any
  style?: React.CSSProperties
}

export const VideoCard: FC<IComponentProps> = ({ data, style }) => {
  return <StyledCard data={data} style={style} />
}
