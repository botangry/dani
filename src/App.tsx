import React, { FC } from 'react'
import styled from 'styled-components'
import { Header } from './components/Header/Header'
import { Footer } from './components/Footer/Footer'
import { Pages } from './components/Pages/Pages'

const AppWrapper = styled.div`
  min-height: 100vh;

  background-color: ${(p) => p.theme.color.main};

  display: flex;
  flex-flow: column nowrap;

  position: relative;
`

export const App: FC = () => (
  <AppWrapper>
    <Header />

    <Pages />

    <Footer />
  </AppWrapper>
)
