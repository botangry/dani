import React, { FC } from 'react'
import styled from 'styled-components'

const Wrapper = styled.a`
  margin-top: 1rem;
  padding: 0.75rem;

  font-size: 0.8rem;
  font-weight: bold;

  color: ${(p) => p.theme.color.btnLink};
  text-decoration: none;
  background-color: ${(p) => p.theme.color.accent};
  border-radius: 6px;
  box-shadow: 0 0 2px ${(p) => p.theme.color.accent};

  display: flex;
  justify-content: center;
  align-content: center;
  transition: 0.2s;

  :hover, :focus {
    color: ${(p) => p.theme.color.accent};

    background-color: transparent;
    box-shadow: inset 0 0 0 2px ${(p) => p.theme.color.accent};
    transition: 0.2s;
  }

  @media (max-width: 900px) {
    margin-top: 0.5rem;
    padding: 0.5rem;
  }
`

interface IComponentProps {
  url: string
  children?: string
}

export const Link: FC<IComponentProps> = ({ url, children = 'More →' }) => (
  <Wrapper href={url} children={children} />
)
