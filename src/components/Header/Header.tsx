import React, { FC } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { routes } from '../../routes/routes'

const HeaderWrapper = styled.header`
  padding: 1rem 2rem;

  background-color: ${(p) => p.theme.color.secondary};
  border-radius: ${(p) => p.theme.borderRadius};

  position: fixed;
  top: 0;
  right: 0;
  z-index: 10;
  transform: translate(-12.5%, 25%);

  @media (max-width: 900px) {
    padding: 0.5rem 1rem;

    font-size: 0.9rem;
  }
`

const LinksWrapper = styled.nav`
  ul {
    height: 100%;
    display: flex;
    flex-flow: row nowrap;
    justify-content: center;
    align-items: center;

    li {
      margin-right: 1rem;
      display: flex;
      justify-content: center;
      align-items: center;

      :last-child {
        margin-right: 0;
      }
    }
  }

  a {
    color: aliceblue;
    text-decoration: none;

    :hover {
      color: cornflowerblue;
    }
  }
`

interface IComponentProps {}

export const Header: FC<IComponentProps> = () => {
  return (
    <HeaderWrapper>
      <LinksWrapper>
        <ul>
          <li>
            <Link to={routes.home} children="Home" />
          </li>

          <li>
            <Link to={routes.games} children="Games" />
          </li>

          <li>
            <Link to={routes.videos} children="Videos" />
          </li>
        </ul>
      </LinksWrapper>
    </HeaderWrapper>
  )
}
