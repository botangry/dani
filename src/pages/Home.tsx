import React, { FC } from 'react'
import styled from 'styled-components'
import { Logo } from '../components/Logo/Logo'

const Wrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
`

interface IComponentProps {}

export const Home: FC<IComponentProps> = () => {
  return (
    <Wrapper>
      <h1>Student. Gamedev. Youtube.</h1>

      <Logo />
    </Wrapper>
  )
}
